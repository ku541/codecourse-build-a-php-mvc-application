<?php

class App
{
    protected $controller = 'Home';
    protected $method = 'index';
    protected $params = [];

    public function __construct()
    {
        $url = $this->parseUrl();

        $file = ucfirst(strtolower($url[0]));

        if (file_exists('../app/controllers/' . $file . '.php')) {
            $this->controller = ucfirst($file);

            unset($url[0]);
        }

        require_once '../app/controllers/' . $this->controller . '.php';

        $this->controller = new $this->controller;

        if (isset(
            $url[1]) &&
            method_exists($this->controller, strtolower($url[1]))
        ) {
            $this->method = strtolower($url[1]);

            unset($url[1]);
        }

        $this->params = $url ? array_values($url) : [];

        call_user_func_array(
            [
            $this->controller, $this->method],
            $this->params
        );
    }

    protected function parseUrl()
    {
        if (isset($_GET['url'])) {
            return explode(
                '/',
                filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL)
            );
        }
    }
}
